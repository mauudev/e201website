<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePilotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pilotos', function (Blueprint $table) {
            $table->increments('id');
            $table->string("nombre");
            $table->string("apellido");
            $table->string("email")->unique();
            $table->string("pais");
            $table->string("nickname");
            //$table->enum('type',['normal','admin']);
            $table->integer('rangos_id')->unsigned();
            $table->foreign('rangos_id')->references('id')->on('rangos')->onDelete('cascade');
            $table->rememberToken()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pilotos');
    }
}
