<?php

namespace E201Website;

use Illuminate\Database\Eloquent\Model;

class Rango extends Model
{
    protected $table = 'rangos';
    protected $fillable = ['rango',
              			   'descripcion'];

    public function setDescripcionUAttribute($value){
    	 $this->attributes['descripcion'] = ucwords($value);
    }
}
