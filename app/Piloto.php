<?php

namespace E201Website;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Piloto extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'pilotos';
    protected $fillable = ['nombre',
              					   'apellido',
              					   'email',
              					   'pais',
              					   'nickname',
              					   'rangos_id',
                           'deleted_at'];

    public function setNombreAttribute($value){
    	 $this->attributes['nombre'] = ucwords($value);
    } 
    public function setApellidoAttribute($value){
    	 $this->attributes['apellido'] = ucwords($value);
    } 
}
