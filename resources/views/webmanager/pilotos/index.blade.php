@extends("webmanager.main")
@section("content")
<div class="panel panel-headline">
	<div class="panel-heading">
		<h3 class="panel-title">Pilotos registrados</h3>
		<p class="panel-subtitle">Aca alguna fecha</p>
	</div>
	<div class="panel-body">
		<div class="row">
		<div class="col-md-6 pull-left"><b>Total:</b> {{ $length }} piloto(s) registrado(s)</div>
		<div class="col-md-2 pull-right"><a href="{{ URL::to('pilotos/create') }}" class="btn btn-primary">Nuevo registro</a></div>
		<br><br>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Email</th>
					<th>Pa&iacute;s</th>
					<th>Nickname</th>
					<th>Rango</th>
					<th>Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody>
				@foreach($pilotos as $piloto)
				<tr>
					<td>{{ $piloto->id }}</td>
					<td>{{ $piloto->nombre }}</td>
					<td>{{ $piloto->apellido }}</td>
					<td>{{ $piloto->email }}</td>
					<td>{{ $piloto->pais }}</td>
					<td>{{ $piloto->nickname }}</td>
					<td>{{ $piloto->rangos_id }}</td>
					<td>
						<div class="btn-group" role="group" aria-label="Basic example">
						<a href="{{ URL::to('pilotos/'.$piloto->id) }}" data-balloon="Ver" data-balloon-pos="up" class="btn btn-primary"><i class="fa fa-eye"></i></a>
						<a href="{{ URL::to('pilotos/'.$piloto->id.'/edit') }}" data-balloon="Editar" data-balloon-pos="up" class="btn btn-warning"><i class="fa fa-edit"></i></a>
						<a href="{{ URL::to('pilotos/'.$piloto->id.'/destroy') }}" data-balloon="Eliminar" data-balloon-pos="up" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@include('alerts.success')
		@include('alerts.errors')
		</div>
	</div>
</div>
{{ $pilotos->links("pagination::bootstrap-4")}}
@endsection