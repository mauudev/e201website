<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			{{Form::label('nombre','Nombre:')}}
			{{ Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del piloto','min'=>5]) }}
		</div>
		<div class="col-md-6">
			{{Form::label('apellido','Apellido:',['class'=>'control-label'])}}
			{{ Form::text('apellido',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del piloto','min'=>5]) }}
		</div>
	</div><br><br><br><br>
	<div class="col-md-12">
		<div class="col-md-6">
			{{Form::label('email','Email:')}}
			{{ Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingrese el email del piloto','min'=>5]) }}
		</div>
		<div class="col-md-6">
			{{Form::label('pais','Pa&iacute;s:',['class'=>'control-label'])}}
			{{ Form::text('pais',null,['class'=>'form-control','placeholder'=>'Ingrese el pais del piloto','min'=>5]) }}
		</div>
	</div><br><br><br><br>
	<div class="col-md-12">
		<div class="col-md-6">
			{{Form::label('nickname','Nickname:')}}
			{{ Form::text('nickname',null,['class'=>'form-control','placeholder'=>'Ingrese el nickname del piloto','min'=>5]) }}
		</div>
		<div class="col-md-6">
			{{Form::label('rango','Rango:',['class'=>'control-label'])}}
			{{ Form::select('rangos_id',$rangos, null,['class'=>'form-control','placeholder'=>'Seleccione un rango..']) }}
		</div>
	</div>
</div><br>
