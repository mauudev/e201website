@extends("webmanager.main")
@section("content")
<div class="panel panel-headline">
	<div class="panel-heading">
		<h3 class="panel-title">Registrar nuevo piloto</h3>
		<p class="panel-subtitle">Aca alguna fecha</p>
	</div>
	<div class="panel-body">
		<div class="row">
			{{ Form::open(['route'=>'pilotos.store','method'=>'POST','class'=>'form-horizontal row-fluid']) }}
		@include("alerts.success")
		@include("webmanager.pilotos.form.registerform")

		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		{!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default" href="{!! URL::to('pilotos') !!}">Cancelar</a>
	</div>
</div>
{{ Form::close() }}
@endsection
